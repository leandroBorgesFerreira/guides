package br.com.leandro.guides.utils

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import br.com.leandro.guides.modules.reviews.paging.RxImmediateSchedulerRule
import org.junit.ClassRule

abstract class AsyncTest {
    companion object {

        const val ANY_RESOURCE_STRING = "Any resource string"
        const val ANY_SUCCESS_MESSAGE = "Success"

        @ClassRule
        @JvmField
        val rule = InstantTaskExecutorRule()

        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }
}
