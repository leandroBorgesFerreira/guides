package br.com.leandro.guides.modules.reviews.paging

import br.com.leandro.guides.modules.reviews.model.Review
import br.com.leandro.guides.repositories.ReviewsRepository
import br.com.leandro.guides.utils.AsyncTest
import br.com.leandro.guides.utils.NetworkState
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.TestObserver
import junit.framework.Assert.assertEquals
import junit.framework.Assert.fail
import org.junit.Test

class ReviewsDataProviderTest: AsyncTest() {

    private val reviews: List<Review> = getDefaultReviewList()

    private val compositeDisposable: CompositeDisposable = mock()
    private val reviewsRepository: ReviewsRepository = mock {
        on(it.getReviews(any(), any())) doReturn Single.just(reviews)
    }

    private val reviewsDataProvider = ReviewsDataProvider(reviewsRepository, compositeDisposable)

    @Test
    fun `proves that network state is correctly propagated`() {
        whenever(reviewsRepository.getReviews(any(), any())) doReturn Single.error(Exception("oh!"))
        val testObserver: TestObserver<NetworkState> = reviewsDataProvider.networkObservable.test()

        reviewsDataProvider.loadData(0, 0) {}

        testObserver.assertValues(NetworkState.RUNNING, NetworkState.FAILED)
    }

    @Test
    fun `proves that data is sent to callback function`() {
        var shouldFail = true

        reviewsDataProvider.loadData(0, 0) { reviews ->
            shouldFail = false
            assertEquals(this.reviews, reviews)
        }

        if (shouldFail) {
            fail()
        }
    }

    @Test
    fun `proves that disposable is added to compositeDisposable`() {
        reviewsDataProvider.loadData(0, 0) {}

        verify(compositeDisposable).add(any())
    }
}
