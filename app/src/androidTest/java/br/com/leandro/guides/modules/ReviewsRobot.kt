package br.com.leandro.guides.modules

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import br.com.leandro.guides.R
import br.com.leandro.guides.modules.reviews.model.Review
import br.com.leandro.guides.utils.atPosition

class ReviewsRobot {

    fun isReviewDisplayedAtPosition(review: Review, position: Int) {
        onView(withId(R.id.reviewsRV))
            .check(matches(atPosition(position, hasDescendant(withText(review.title)))))

        onView(withId(R.id.reviewsRV))
            .check(matches(atPosition(position, hasDescendant(withText(review.rating.toString())))))

        onView(withId(R.id.reviewsRV))
            .check(matches(atPosition(position, hasDescendant(withText(review.message)))))
    }

    fun isMessageShown(message: String) {
        onView(withText(message)).check(matches(isDisplayed()))
    }

    fun isLoadingSpinnerShowing() {
        onView(withId(R.id.progressBar)).check(matches(isDisplayed()))
    }
}

fun test(func: ReviewsRobot.() -> Unit) = ReviewsRobot().apply(func)
