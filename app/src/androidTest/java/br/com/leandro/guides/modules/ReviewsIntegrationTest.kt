package br.com.leandro.guides.modules

import android.content.Intent
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import br.com.leandro.guides.R
import br.com.leandro.guides.getDefaultRequestDto
import br.com.leandro.guides.modules.reviews.ReviewsActivity
import br.com.leandro.guides.utils.startWithDefaultPort
import com.google.gson.Gson
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ReviewsIntegrationTest {

    @Rule
    @JvmField
    var testRule = ActivityTestRule(ReviewsActivity::class.java, true, false)

    private val server = MockWebServer()

    private val reviewList = getDefaultRequestDto().data

    @Before
    fun setUp() {
        server.startWithDefaultPort()
    }

    @Test
    fun shouldDisplayCorrectlyTheInformation() {
        with(server) {
            enqueue(MockResponse().setBody(Gson().toJson(getDefaultRequestDto())).setResponseCode(200))
            enqueue(MockResponse().setBody(Gson().toJson(getDefaultRequestDto())).setResponseCode(200))
            enqueue(MockResponse().setBody(Gson().toJson(getDefaultRequestDto())).setResponseCode(200))
        }

        testRule.launchActivity(Intent())

        test {
            isReviewDisplayedAtPosition(reviewList[0], 0)
            isReviewDisplayedAtPosition(reviewList[1], 1)
            isReviewDisplayedAtPosition(reviewList[2], 2)
        }
    }

    @Test
    fun shouldShowError() {
        server.enqueue(MockResponse().setBody(Gson().toJson(getDefaultRequestDto())).setResponseCode(404))
        val activity = testRule.launchActivity(Intent())

        test {
            isMessageShown(activity.getText(R.string.default_error_message).toString())
        }
    }

    @Test
    fun shouldShowLoading() {
        server.enqueue(MockResponse().setBody(Gson().toJson(getDefaultRequestDto())).setResponseCode(200))
        testRule.launchActivity(null)

        test {
            isLoadingSpinnerShowing()
        }
    }

    @After
    fun tearDown() {
        server.shutdown()
    }
}
