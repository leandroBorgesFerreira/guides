package br.com.leandro.guides.utils

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import br.com.leandro.guides.TestApplication

class CustomTestRunner : AndroidJUnitRunner() {

    override fun newApplication(cl: ClassLoader?, className: String?, context: Context?): Application =
        super.newApplication(cl, TestApplication::class.java.name, context)
}
