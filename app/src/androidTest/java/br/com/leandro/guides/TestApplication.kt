package br.com.leandro.guides

import br.com.leandro.guides.application.GuidesApplication
import br.com.leandro.guides.dagger.ApplicationModule
import br.com.leandro.guides.dagger.DaggerInjectorComponent
import br.com.leandro.guides.dagger.NetModule
import br.com.leandro.guides.utils.MOCK_SERVER_PORT

class TestApplication : GuidesApplication() {

    override fun onCreate() {
        super.onCreate()

        DaggerInjectorComponent.builder()
            .netModule(NetModule("http://localhost:${MOCK_SERVER_PORT}/"))
            .applicationModule(ApplicationModule(this))
            .build()
            .inject(this)
    }
}
