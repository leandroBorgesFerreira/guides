package br.com.leandro.guides.utils

import okhttp3.mockwebserver.MockWebServer

fun MockWebServer.startWithDefaultPort() =
    this.start(MOCK_SERVER_PORT)
