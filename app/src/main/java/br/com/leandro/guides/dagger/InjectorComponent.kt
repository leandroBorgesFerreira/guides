package br.com.leandro.guides.dagger

import br.com.leandro.guides.application.GuidesApplication
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ActivityBinder::class,
    NetModule::class,
    AndroidSupportInjectionModule::class,
    ApplicationModule::class
])
interface InjectorComponent {

    fun inject(customApplication: GuidesApplication)

}
