package br.com.leandro.guides.modules.reviews.paging

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import br.com.leandro.guides.R
import br.com.leandro.guides.modules.reviews.model.Review
import br.com.leandro.guides.utils.NetworkState
import kotlinx.android.synthetic.main.loading_item.view.*
import kotlinx.android.synthetic.main.review_item.view.*

private const val TYPE_PROGRESS = 0
private const val TYPE_ITEM = 1

class ReviewsPagedAdapter(
    private val context: Context
) : PagedListAdapter<Review, RecyclerView.ViewHolder>(Review.DIFF_CALLBACK) {

    private var networkState = NetworkState.RUNNING

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        LayoutInflater.from(context)
            .run {
                if (viewType == TYPE_ITEM) {
                    inflate(R.layout.review_item, parent, false).let(::ReviewsViewHolder)
                } else {
                    inflate(R.layout.loading_item, parent, false).let(::NetworkStateItemViewHolder)
                }
            }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
        when (holder) {
            is ReviewsViewHolder -> holder.bind(getItem(position)!!)
            is NetworkStateItemViewHolder -> holder.bindLoading(networkState)
            else -> throw IllegalStateException("wrong view holder!!")
        }

    fun setNetworkState(networkState: NetworkState) {
        val hadExtraRow = hasExtraRow(this.networkState)
        val hasExtraRowNow = hasExtraRow(networkState)
        if (hadExtraRow != hasExtraRowNow) {
            if (hadExtraRow) {
                notifyItemRemoved(itemCount)
            } else {
                notifyItemInserted(itemCount)
            }
        }

        this.networkState = networkState
    }

    override fun getItemViewType(position: Int): Int =
        if (networkState == NetworkState.RUNNING && position == itemCount - 1) {
            TYPE_PROGRESS
        } else {
            TYPE_ITEM
        }

    private fun hasExtraRow(networkState: NetworkState): Boolean = networkState != NetworkState.SUCCESS

    internal class ReviewsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(review: Review) {
            itemView.run {
                reviewTitle.text = if (!review.title.isNullOrBlank()) {
                    review.title
                } else {
                    context.getText(R.string.no_title)
                }

                reviewGrade.text = review.rating.toString()

                reviewMessage.text = if (!review.message.isNullOrBlank()) {
                    review.message
                } else {
                    context.getText(R.string.no_message)
                }

                reviewInfo.text = "${review.author} - ${review.reviewerCountry} - ${review.date}"
            }
        }
    }

    internal class NetworkStateItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindLoading(networkState: NetworkState) {
            itemView.run {
                when (networkState) {
                    NetworkState.RUNNING -> progressBar.visibility = View.VISIBLE
                    NetworkState.SUCCESS -> progressBar.visibility = View.GONE
                    NetworkState.FAILED -> {
                        errorMessage.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                    }
                }
            }
        }
    }
}
