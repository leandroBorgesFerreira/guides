package br.com.leandro.guides.utils

import br.com.leandro.guides.common.RefreshingView
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

fun Disposable.addToDisposableBag(compositeDisposable: CompositeDisposable) {
    compositeDisposable.add(this)
}

fun <T> Observable<T>.load(view: RefreshingView): Observable<T> =
    this.doOnSubscribe { view.showRefreshing(true) }
        .doOnNext { view.showRefreshing(false) }

fun <T> Single<T>.load(view: RefreshingView): Single<T> =
    this.doOnSubscribe { view.showRefreshing(true) }
        .doOnError { view.showRefreshing(false) }
        .doOnSuccess { view.showRefreshing(false) }
