package br.com.leandro.guides.modules.reviews.model

import com.google.gson.annotations.SerializedName

data class ReviewsRequestDto(
    @SerializedName("status") val status: Boolean,
    @SerializedName("total_reviews_comments") val totalReviewsComments: Long,
    @SerializedName("data") val data: List<Review>
)
