package br.com.leandro.guides.common

interface RefreshingView {
    fun showRefreshing(isRefreshing: Boolean)
    fun showError()
}
