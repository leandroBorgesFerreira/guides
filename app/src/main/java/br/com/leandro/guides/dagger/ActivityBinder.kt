package br.com.leandro.guides.dagger

import br.com.leandro.guides.modules.reviews.ReviewsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBinder {

    @ContributesAndroidInjector(modules = [])
    abstract fun bindFormView(): ReviewsActivity

}
