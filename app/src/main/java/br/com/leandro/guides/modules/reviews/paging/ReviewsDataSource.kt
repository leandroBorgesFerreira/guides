package br.com.leandro.guides.modules.reviews.paging

import androidx.paging.PageKeyedDataSource
import br.com.leandro.guides.modules.reviews.model.Review

class ReviewsDataSource(private val reviewsDataProvider: ReviewsDataProvider) : PageKeyedDataSource<Long, Review>() {

    override fun loadInitial(params: LoadInitialParams<Long>, callback: LoadInitialCallback<Long, Review>) {
        reviewsDataProvider.loadData(params.requestedLoadSize, 0) { reviews ->
            callback.onResult(reviews, null, 1)
        }
    }

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, Review>) {
        reviewsDataProvider.loadData(params.requestedLoadSize, params.key.toInt()) { reviews ->
            callback.onResult(reviews, params.key + 1)
        }
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, Review>) {
        //No need for. We start at page 0
    }
}


