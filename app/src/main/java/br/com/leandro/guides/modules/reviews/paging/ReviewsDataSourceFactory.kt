package br.com.leandro.guides.modules.reviews.paging

import androidx.paging.DataSource
import br.com.leandro.guides.modules.reviews.model.Review
import br.com.leandro.guides.utils.NetworkState
import io.reactivex.Observable

class ReviewsDataSourceFactory(reviewsDataProvider: ReviewsDataProvider) : DataSource.Factory<Long, Review>() {

    private val dataSource = ReviewsDataSource(reviewsDataProvider)

    val errorObservable: Observable<NetworkState> = reviewsDataProvider.networkObservable.hide()

    override fun create(): DataSource<Long, Review> = dataSource

}
