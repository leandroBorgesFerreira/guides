package br.com.leandro.guides.modules.reviews

import androidx.paging.PagedList
import androidx.paging.RxPagedListBuilder
import br.com.leandro.guides.modules.reviews.model.Review
import br.com.leandro.guides.modules.reviews.paging.ReviewsDataProvider
import br.com.leandro.guides.modules.reviews.paging.ReviewsDataSourceFactory
import br.com.leandro.guides.repositories.ReviewsRepository
import br.com.leandro.guides.utils.NetworkState
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ReviewsViewModel @Inject constructor(private val reviewsRepository: ReviewsRepository) {

    private val pagedListConfig = PagedList.Config.Builder()
        .setEnablePlaceholders(false)
        .setInitialLoadSizeHint(20)
        .setPageSize(10)
        .build()

    fun articleListObservable(compositeDisposable: CompositeDisposable):
        Pair<Observable<NetworkState>, Observable<PagedList<Review>>> {
        val factory = ReviewsDataSourceFactory(ReviewsDataProvider(reviewsRepository, compositeDisposable))

        return factory.errorObservable to RxPagedListBuilder(factory, pagedListConfig)
            .setFetchScheduler(Schedulers.io())
            .buildObservable()
    }

}
