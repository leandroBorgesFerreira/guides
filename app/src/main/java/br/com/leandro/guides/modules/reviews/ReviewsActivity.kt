package br.com.leandro.guides.modules.reviews

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.leandro.guides.R
import br.com.leandro.guides.common.RefreshingView
import br.com.leandro.guides.modules.reviews.paging.ReviewsPagedAdapter
import br.com.leandro.guides.utils.NetworkState
import br.com.leandro.guides.utils.addToDisposableBag
import br.com.leandro.guides.utils.load
import dagger.android.DaggerActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class ReviewsActivity : DaggerActivity(), RefreshingView {

    @Inject
    lateinit var viewModel: ReviewsViewModel

    private val adapter = ReviewsPagedAdapter(this)

    private val compositeDisposable = CompositeDisposable()

    private var dialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        reviewsRV.adapter = adapter
        reviewsRV.layoutManager = LinearLayoutManager(this)

        val requestPair = viewModel.articleListObservable(compositeDisposable)

        requestPair.first
            .filter { networkState -> networkState == NetworkState.FAILED }
            .subscribe { networkState ->
                if (networkState == NetworkState.FAILED) {
                    showError()
                } else {
                    adapter.setNetworkState(networkState)
                }
            }
            .addToDisposableBag(compositeDisposable)

        requestPair.second
            .observeOn(AndroidSchedulers.mainThread())
            .load(this)
            .subscribe(adapter::submitList) { showError() }
            .addToDisposableBag(compositeDisposable)
    }


    override fun showRefreshing(isRefreshing: Boolean) {
        if (isRefreshing) {
            progressLayout.visibility = View.VISIBLE
        } else {
            progressLayout.visibility = View.GONE
        }
    }

    override fun showError() {
        dialog = AlertDialog.Builder(this)
            .setMessage(getText(R.string.default_error_message))
            .setPositiveButton("OK") { dialogInterface, _ -> dialogInterface.dismiss() }
            .show()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
        dialog?.dismiss()
        dialog = null
    }
}
