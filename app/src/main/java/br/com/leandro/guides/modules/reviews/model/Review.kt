package br.com.leandro.guides.modules.reviews.model

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Review(
    @SerializedName("review_id") val reviewId: Long,
    @SerializedName("rating") val rating: Float,
    @SerializedName("title") val title: String?,
    @SerializedName("message") val message: String?,
    @SerializedName("author") val author: String?,
    @SerializedName("date") val date: String?,
    @SerializedName("reviewerCountry") val reviewerCountry: String?
) : Parcelable {

    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<Review> = object : DiffUtil.ItemCallback<Review>() {
            override fun areItemsTheSame(oldItem: Review, newItem: Review): Boolean =
                oldItem.reviewId == newItem.reviewId

            override fun areContentsTheSame(oldItem: Review, newItem: Review): Boolean =
                oldItem == newItem

        }
    }
}
