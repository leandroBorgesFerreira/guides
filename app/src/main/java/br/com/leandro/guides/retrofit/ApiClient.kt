package br.com.leandro.guides.retrofit

import br.com.leandro.guides.modules.reviews.model.ReviewsRequestDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiClient {

    @GET("{city}/{tour}/reviews.json")
    fun getReviews(
        @Path(value = "city") city: String,
        @Path(value = "tour") tour: String,
        @Query("count") count: Int,
        @Query("page") page: Int,
        @Query("rating") rating: Int,
        @Query("sortBy") sortBy: String,
        @Query("direction") direction: String
    ): Single<ReviewsRequestDto>

}
