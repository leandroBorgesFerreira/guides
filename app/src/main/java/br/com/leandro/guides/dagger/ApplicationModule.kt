package br.com.leandro.guides.dagger

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule(private val application: Application) {

    @Provides
    fun provideApplication() = application

    @Provides
    fun provideContext(): Context = application

}
