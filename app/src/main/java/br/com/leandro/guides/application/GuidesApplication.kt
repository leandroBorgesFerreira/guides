package br.com.leandro.guides.application

import android.app.Activity
import android.app.Application
import br.com.leandro.guides.dagger.ApplicationModule
import br.com.leandro.guides.dagger.DaggerInjectorComponent
import br.com.leandro.guides.dagger.NetModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

private const val API_URL = "https://www.getyourguide.com/"

open class GuidesApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Activity>

    var baseUrl = API_URL

    override fun onCreate() {
        super.onCreate()

        DaggerInjectorComponent.builder()
            .netModule(NetModule(baseUrl))
            .applicationModule(ApplicationModule(this))
            .build()
            .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = androidInjector

}
