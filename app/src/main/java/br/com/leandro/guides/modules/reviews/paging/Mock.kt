package br.com.leandro.guides.modules.reviews.paging

import br.com.leandro.guides.modules.reviews.model.Review
import br.com.leandro.guides.modules.reviews.model.ReviewsRequestDto

fun getDefaultRequestDto() = ReviewsRequestDto(true, 3, getDefaultReviewList())

fun getDefaultReview() = Review(1, 3.7F, "title", "message", "Leandro", "20-03-2020", "Brasil")
fun getDefault1Review() = Review(2, 3.5F, "title1", "message1", "Leandro", "20-03-2020", "Brasil")
fun getDefault2Review() = Review(3, 3.5F, "title2", "message2", "Leandro", "20-03-2020", "Brasil")
fun getDefault3Review() = Review(4, 4.5F, "title3", "message3", "Leandro", "20-03-2020", "Brasil")

fun getDefaultReviewList() = listOf(
    getDefaultReview(),
    getDefault1Review(),
    getDefault2Review(),
    getDefault3Review()
)
