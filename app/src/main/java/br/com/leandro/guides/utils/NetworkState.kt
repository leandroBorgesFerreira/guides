package br.com.leandro.guides.utils

enum class NetworkState {
    RUNNING,
    SUCCESS,
    FAILED
}
