package br.com.leandro.guides.repositories

import br.com.leandro.guides.modules.reviews.model.Review
import br.com.leandro.guides.modules.reviews.paging.getDefaultRequestDto
import br.com.leandro.guides.retrofit.ApiClient
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ReviewsRepository @Inject constructor(private val apiClient: ApiClient) {

    fun getReviews(count: Int, page: Int): Single<List<Review>> = apiClient.getReviews(
        city = "berlin-l17",
        tour = "tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776",
        count = count,
        page = page,
        rating = 0,
        sortBy = "date_of_review",
        direction = "DESC")
        .map { dto -> dto.data }

    fun getMockedReviews(count: Int, page: Int): Single<List<Review>> = Single.just(getDefaultRequestDto().data)
        .delay(1, TimeUnit.SECONDS)
}
