package br.com.leandro.guides.modules.reviews.paging

import br.com.leandro.guides.modules.reviews.model.Review
import br.com.leandro.guides.repositories.ReviewsRepository
import br.com.leandro.guides.utils.NetworkState
import br.com.leandro.guides.utils.addToDisposableBag
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject

class ReviewsDataProvider(
    private val reviewsRepository: ReviewsRepository,
    private val compositeDisposable: CompositeDisposable
) {

    val networkObservable = BehaviorSubject.create<NetworkState>()

    fun loadData(loadSize: Int, page: Int, resultFunction: (List<Review>) -> Unit) {
        reviewsRepository.getReviews(loadSize, page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .networkStateObservable(networkObservable)
            .subscribe(resultFunction, {
                networkObservable.onNext(NetworkState.FAILED)
            }).addToDisposableBag(compositeDisposable)
    }

    private fun <T> Single<T>.networkStateObservable(subject: Subject<NetworkState>): Single<T> =
        this.doOnSubscribe { subject.onNext(NetworkState.RUNNING) }
            .doOnSuccess { subject.onNext(NetworkState.SUCCESS) }
}
