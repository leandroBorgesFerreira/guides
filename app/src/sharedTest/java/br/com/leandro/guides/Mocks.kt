package br.com.leandro.guides

import br.com.leandro.guides.modules.reviews.model.Review
import br.com.leandro.guides.modules.reviews.model.ReviewsRequestDto

fun getDefaultRequestDto() = ReviewsRequestDto(true, 3, getDefaultReviewList())

fun getDefaultReview() = Review(1, 3.7F, "title", "message", null, null, null)
fun getDefault1Review() = Review(2, 3.5F, "title1", "message1", null, null, null)
fun getDefault2Review() = Review(3, 3.5F, "title2", "message2", null, null, null)
fun getDefault3Review() = Review(4, 4.5F, "title3", "message3", null, null, null)

fun getDefaultReviewList() = listOf(
    getDefaultReview(),
    getDefault1Review(),
    getDefault2Review(),
    getDefault3Review()
)
