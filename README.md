# GetYourGuide Test

This document explains my solution and decisions about how I made the test.

### Architecture

I've chosen to use MVVM to this project. It avoids memory leaks by not holding a reference of the Activity in the presentation layer. 

I am using Dagger for dependency injection. Although it is not necessary in a small project like this one, it would be good to have this dependency in place in case this project had to scaled. 

The communication between layer is make using RxJava. RxJava helps to deal with asynchronicity in a more simple way and avoid the need to keep references between layer. 

### The solution

My app is a simple infite scrol. It allows the user to browse all the reviews for the given tour. There's a toolbar that hides when the user scrols the screen to release room for the information. 

I've chosen the android paging library because it has a clean API and it goes easy on the device resources. 

### Tests

I used the [robot pattern](https://academy.realm.io/posts/kau-jake-wharton-testing-robots/) for UI tests. And I separated my logic from the android SDK to create unit tests, although test's not much to be tested with unit tests in this project. 

### How to Build the project

In order to see the data in the project change the call of `reviewsRepository` inside the `ReviewsDataProvider` from `reviewsRepository.getReviews(loadSize, page)` to `reviewsRepository.getMockedReviews(loadSize, page)` and you'll be able to navigate in the project